
echo "Removing protection..."
sleep 1
openocd -f interface/stlink-v2.cfg -f target/stm32f1x.cfg -c "init" -c "halt" -c "stm32f1x unlock 0" -c "shutdown"
echo "Done!"

sleep 4
echo "Erasing... Flashing DFU + firmware..."
sleep 1
st-flash erase
st-flash write blackmagic_dfu.bin 0x8000000
st-flash --reset write blackmagic.bin 0x8002000
echo "Done!"
