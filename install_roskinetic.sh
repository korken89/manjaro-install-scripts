
yaourtpkgs="\
    python2-pip \
    python2-rosinstall-generator \
    console-bridge \
    python2-rosdep \
    ogre \
    apr \
    collada-dom \
    python2-matplotlib \
    python2-nose \
    python2-defusedxml \
    tango-icon-theme \
    python2-docutils \
    sbcl \
    python2-paramiko \
    python2-pillow \
    vtk \
    poco \
    python2-psutil \
    yaml-cpp \
    python2-netifaces \
    python2-opengl \
    eigen \
    python2-pygraphviz \
    screen \
    python2-sip \
    python2-empy \
    apr-util
    python2-pyqt5 \
    python2-coverage \
    urdfdom-headers \
    hddtemp \
    tinyxml \
    graphviz \
    python2-pydot \
    python2-numpy \
    cppunit \
    urlgrabber \
    python2-mock \
    log4cxx \
    urdfdom \
    python2-catkin-tools \
    assimp \
    "

# Install some needed package
echo Installing from yaourt: $yaourtpkgs
sleep 1

# We need GCC here for some packages to compile...
export CC="/usr/bin/gcc"
export CXX="/usr/bin/g++"

yaourt -S --noconfirm --needed $yaourtpkgs

# And for the rest we need Clang for some other to compile...
# export CC="/usr/bin/clang"
# export CXX="/usr/bin/clang++"

echo Install wstool
sleep 1
sudo pip2 install wstool

echo ROSdep init
sleep 1
sudo rosdep init
rosdep update

echo Changing folder...
sleep 1
mkdir -p ~/rosinstall_kinetic
cd ~/rosinstall_kinetic

echo Generate install file
sleep 1
rosinstall_generator desktop image_geometry multimaster_fkie --rosdistro kinetic --deps --wet-only --tar > kinetic-desktop-wet.rosinstall

echo Getting sources
sleep 1
wstool init -j8 src kinetic-desktop-wet.rosinstall

echo Changing to python 2...
sleep 1
py2

echo Starting compile!
sleep 1
./src/catkin/bin/catkin_make_isolated --install -DCMAKE_BUILD_TYPE=Release -DENABLE_PRECOMPILED_HEADERS=Off -DCMAKE_C_COMPILER=/usr/bin/gcc -DCMAKE_CXX_COMPILER=/usr/bin/g++ -j
