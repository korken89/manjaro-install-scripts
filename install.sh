# Stolen from Krille :D
# And made better ;)

packages="\
    neovim \
    python2-neovim \
    python-neovim \
    git \
    tk \
    firefox \
    arm-none-eabi-gcc \
    arm-none-eabi-gdb \
    arm-none-eabi-newlib \
    stlink \
    clang \
    clang-tools-extra \
    lldb \
    cmake \
    minicom \
    ack \
    network-manager-applet \
    pavucontrol \
    pulseaudio \
    pulseaudio-bluetooth \
    pulseaudio-alsa \
    pulseaudio-ctl \
    urxvt-perls \
    boost \
    boost-libs \
    gtest \
    google-glog \
    eigen \
    opencv \
    opencv-samples \
    youtube-viewer \
    youtube-dl \
    texlive-most \
    evince \
    dropbox \
    xclip \
    gtk3-print-backends \
    mercurial \
    okular \
    atom \
    kicad \
    kicad-library \
    kicad-library-3d \
    qt \
    qtcreator \
    "

yaourtpkgs="\
    telegram-desktop-bin \
    neovim-symlinks \
    openocd-git \
    qstlink2-git \
    avr-gcc \
    avr-binutils \
    avr-gdb \
    avr-libc \
    avrdude \
    arduino \
    arduino-mk \
    nlopt \
    otf-inconsolata-powerline-git \
    spotify \
    latex-mk \
    j4-dmenu-desktop \
    skype \
    ffmpeg0.10 \
    google-talkplugin \
    slack-desktop \
    ncurses5-compat-libs \
    "

atompkgs="\
    atom-gdb-debugger \
    build\
    build-make \
    busy \
    clang-format \
    git-plus \
    highlight-line \
    linter \
    linter-clang \
    monokai \
    vim-mode-plus \
    you-complete-me \
    tabs-to-spaces \
    language-vhdl \
    vhdl-entity-converter \
    "

#services="""
#sshd.socket
#libvirtd
#virtlogd.socket
#"""

removeables="\
    palemoon-bin \
    modemmanager \
    vim \
    vi \
    "
# Install some needed package
echo Removing some software: $removeables
sleep 1
sudo pacman -Rsc $removeables

echo Fixing mirrors!
sleep 1
sudo pacman-mirrors -g

echo Upgrading...
sleep 1
sudo pacman -Syu --noconfirm

echo Installing: $packages
sleep 1
sudo pacman -S --noconfirm --needed $packages

echo Fixing keys for yaourt packages...
sleep 1
gpg --recv-keys 702353E0F7E48EDB

echo Installing from yaourt: $yaourtpkgs
sleep 1
yaourt -S --noconfirm --needed $yaourtpkgs

echo Installing Atom packages
sleep 1
apm install $atompkgs

# enable some stuff
#echo enabling: $services
#sudo systemctl enable $services
#sudo systemctl start $services

# fix udev rules

user=$(whoami)
echo Setting groups for user: $user
sleep 1
# set some groups for manjaro and arch
sudo usermod -aG wheel $user
sudo usermod -aG uucp $user

# Fix ltu printer system
echo Fixing LTU printer...
sleep 1
sudo sh -c 'echo "ServerName IPP.LTU.SE" > /etc/cups/client.conf'

# Fix config files
echo Fixing some configs...
sleep 1

if [ -f ~/.Xresources ]; then
    echo ""
    echo ".Xresources already exists, moved to .Xresources_old."
    mv ~/.Xresources ~/.Xresources_old
fi
ln -sf "$(pwd)/i3_config" ~/.i3/config
ln -sf "$(pwd)/i3status.conf" ~/.i3status.conf
ln -sf "$(pwd)/Xresources" ~/.Xresources
ln -sf "$(pwd)/latexmkrc" ~/.latexmkrc
echo "[[ -f '$(pwd)/bashrc_additions' ]] && source $(pwd)/bashrc_additions " >> ~/.bashrc
ln -sf "$(pwd)/ymusic.sh" ~/.ymusic.sh
ln -sf "$(pwd)/atom-config.cson" ~/.atom/config.cson
sudo ln -sf "$(pwd)/lock.sh" /usr/bin/pixellock
ln -sf "$(pwd)/gdbinit" ~/.gdbinit

# Fix vim settings
echo Fixing some nvim configs...
sleep 1
git clone https://github.com/korken89/nvim.git ~/.config/nvim
nvim -c PlugInstall

# Copying udec rules
echo Copying udev rules...
sleep 1
sudo cp ./udev_rules/* /etc/udev/rules.d/
sudo udevadm control --reload
