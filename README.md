# A collection of install scripts for Manjaro

### Install instructions

1. Download the repository to .config/...
2. Run the `./install.sh` - WITHOUT sudo
3. ???
4. Profit!

One-liner: `sudo pacman -S git --noconfirm && git clone https://gitlab.com/korken89/manjaro-install-scripts.git ~/.config/manjaro-install-scripts && cd ~/.config/manjaro-install-scripts && bash -C install.sh`
