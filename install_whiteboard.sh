#
# This will install a digital whiteboard together with the Wacom drivers.
#

packages="\
    xournal \
    xf86-input-wacom \
    "
yaourtpkgs="\
    kazam \
    "

echo "Installing digital whiteboard + drivers"
sleep 1
sudo pacman -S $packages --needed --noconfirm
yaourt -S $yaourtpkgs --needed --noconfirm
